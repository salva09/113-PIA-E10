# Automaton theory final project

Description

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
Node.js #See the web page in built in to install it
Electron #Use the command npm install electron -g
```

### Trying the project

A step by step series of how run this project in your computer

```
# Clone the repository
$ git clone https://github.com/salva09/113-PIA-E10.git
# Go into the repository
$ cd 113-PIA-E10
# Install dependencies
$ npm install
# Run the app
$ npm start
```

## Built With

* [Node.js](https://nodejs.org/en/) - JavaScript runtime
* [Electron](https://www.electronjs.org/) - Framework
* HTML - Programming language
* CSS - Style language
* JavaScript - Programming language

## Authors

* **Salvador Hernandez** - *Initial work* - [salva09](https://github.com/salva09)
